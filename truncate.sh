# truncate history, keeping the initial commit and the last $RETAIN_COMMITS
# https://stackoverflow.com/questions/29914052/how-to-git-rebase-a-branch-with-the-onto-command
TOTAL_COMMITS=$(git rev-list --count HEAD)
OLD_PARENT=$(git rev-list --max-parents=0 HEAD)
if [[ $TOTAL_COMMITS < $RETAIN_COMMITS ]]
then
  RETAIN_COMMITS=$(($TOTAL_COMMITS - 1))
fi
NEW_PARENT=$(git rev-parse HEAD~${RETAIN_COMMITS})

git rebase --onto $OLD_PARENT $NEW_PARENT   # rebase on the new parent
git gc --prune=now --aggressive             # delete the commits
git push origin HEAD:main --force
