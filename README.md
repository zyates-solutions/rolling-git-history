# Rolling Git History

A project with a CI job that autmatically truncates its own history to the last few commits.

## Setup

1. Create a [Project Deploy Key](https://docs.gitlab.com/ee/user/project/deploy_keys/) and name it SSH_PUSH_KEY, give it write access to the repository.
1. Add the private key you generated above to a [CI/CD Variable](https://docs.gitlab.com/ee/ci/variables/) named SSH_PUSH_KEY and mark it protected.
1. [Allow force push](https://docs.gitlab.com/ee/user/project/protected_branches.html#allow-force-push-on-a-protected-branch) on the main branch (if protected)
1. Setup a [pipeline schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) to run the "truncate-history" job
1. ...
1. profit!
